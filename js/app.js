(function(){
	var app = angular.module('store', ['store-products', 'ui.bootstrap']);

	app.controller('StoreController', ['$http', function($http){
		// this.products = beers;
		var store = this;
		store.products = [];

		$http.get('api/products.html').success(function(data){
			store.products = data;
		})
	}]);

    app.controller("ReviewController", function() {
    	this.review = {};

    	this.addReview = function(product){
    		this.review.createdOn = Date.now();
    		product.reviews.push(this.review);
    		this.review = {};
    	};
    });

})();
