(function(){
	var app = angular.module('store-products', [ ]);

	app.directive('productTitle', function(){
		return {
			restrict: 'E',
			templateUrl: 'templates/product/product-title.html'
		};
	});

	app.directive('productPanels', function(){
		return {
			restrict: 'E',
			templateUrl: 'templates/product/product-panels.html',
			controller:function(){
				this.tab = 1;

				this.selectTab = function(setTab) {
					this.tab = setTab;
				};

				this.isSelected = function(checkTab){
					return this.tab === checkTab;
				};
			},
			controllerAs: 'panel'
		};
	});

	app.directive('productGallery', function(){
		return {
			restrict: 'E',
			templateUrl: 'templates/product/product-gallery.html',
			controller: function(){
		  		this.currentImage = 0;

		    	this.setCurrent = function(newCurrent){
		    		this.currentImage = newCurrent || 0;
		      	}
		    },
		    controllerAs: 'gallery'
		};
	});

	app.directive('productDescription', function(){
		return {
			restrict: 'E',
			templateUrl: 'templates/product/product-description.html'
		};
	});

	app.directive('productSize', function(){
		return {
			restrict: 'E',
			templateUrl: 'templates/product/product-size.html'
		};
	});

	app.directive('productFunding', function(){
		return {
			restrict: 'E',
			templateUrl: 'templates/product/product-funding.html'
		};
	});

	app.directive('productReviews', function(){
		return {
			restrict: 'E',
			templateUrl: 'templates/product/product-reviews.html'
		};
	});

})();